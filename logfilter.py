#!/usr/bin/env python
#
# Filter the git log looking for stray Bazaar references

import sys, re

full = True	# Back to True if we think there are bare revnos

for line in sys.stdin:
    if not line[0].isspace():
        continue
    if re.match("    debbugs:[0-9]*$", line):
        continue
    if full:
        skip = False
        for k in ("PURESIZE", "PNG_", "68000", "0:0", "65535", "2000", "0000"):
            if k in line:
                skip = True
                break
        if skip:
            continue
        line = line.replace("POSIX 1003.1-1988", "")
        line = line.replace("gnus--rel--5.10", "")
        line = line.replace("gnus--5.10", "")
        line = line.replace("gnus--rel--5.9", "")
        line = line.replace("gnus--5.9", "")
        line = line.replace("gnus--rel--5.8", "")
        line = line.replace("gnus--5.8", "")
        line = re.sub("http://lists.gnu.org/.*\.html", "", line)
        line = re.sub("http://debbugs.gnu.org/.*\.html", "", line)
        line = re.sub("http://debbugs.gnu.org/[#0-9]*", "", line)
        line = re.sub("[Vv]ersion[ .0-9-]*", "", line)
        line = re.sub(r"[Vv]ersion number\([ .0-9-]*", "", line)
        line = re.sub("[Rr]elease[ .0-9-]*", "", line)
        line = re.sub("GCC[ .0-9]*", "", line)
        line = re.sub("After[ .,0-9]*", "", line)
        line = re.sub("[Oo]rg[ .0-9]*", "", line)
        line = re.sub("[Ee]macs[ .0-9-]*", "", line)
        line = re.sub("Tramp[ .0-9]*", "", line)
        line = re.sub("GFDL[ .0-9]*", "", line)
        line = re.sub("MinGW[ .0-9]*", "", line)
        line = re.sub("GnuPG[ .0-9]*", "", line)
        line = re.sub("libpng[ .0-9]*", "", line)
        line = re.sub("CEDET[ .0-9]*", "", line)
        line = re.sub("automake[ .0-9]*", "", line)
        line = re.sub("CVS[ .0-9]*", "", line)
        line = re.sub("Cygwin[ .0-9]*", "", line)
        line = re.sub("Synched with[ .0-9]*", "", line)
        line = re.sub("signed-bad[ .0-9-]*", "", line)
        line = re.sub("signed-good[ .0-9-]*", "", line)
        line = re.sub(">= [ .0-9]*", "", line)
        line = re.sub("FDL 1.[0-9]*", "", line)
        line = re.sub("OS *X[ .0-9]*", "", line)
        line = re.sub("version to[ .0-9]*", "", line)
        line = re.sub("debbugs:[0-9]+", "", line)
        line = re.sub("[Bb]ugs* *#*[0-9]+", "", line)
        line = re.sub(r"24\.[ .0-9]*", "", line)
        line = re.sub(r"23\.[ .0-9]*", "", line)
        line = re.sub(r"22\.[ .0-9]*", "", line)
        line = re.sub(r"21\.[ .0-9]*", "", line)
        line = re.sub(r"20\.[ .0-9]*", "", line)
        line = re.sub(r"19\.[ .0-9]*", "", line)
        line = re.sub(r"#[0-9]*", "", line)
        line = re.sub(r"0x[0-9]*", "", line)
        line = re.sub(r"x[0-9][0-9]*", "", line)
        line = re.sub(r"/[0-9][0-9]*", "", line)
        line = re.sub(r"@[0-9][0-9]*", "", line)
        line = re.sub(r"=[0-9][0-9]*", "", line)
        line = re.sub(r"_[0-9][0-9]*", "", line)
        line = re.sub(r"-[0-9][0-9][0-9][0-9]*", "", line)
        line = re.sub(r"[a-qsz][0-9][0-9]*", "", line)
        line = re.sub(r"daemon  *[0-9]*", "", line)
        line = re.sub(r"Increased? to *[0-9]*", "", line)
        line = re.sub(r"Reverte?d? to *[0-9]*", "", line)
        line = re.sub(r"Enlarged? to *[0-9]*", "", line)
        line = re.sub(r"Reduced? to *[0-9]*", "", line)
        line = re.sub(r"gb18030", "", line)
        line = re.sub(r"cp51932", "", line)
        line = re.sub(r"is13194", "", line)
        line = re.sub(r"ISO[0-9]*", "", line)
        line = re.sub(r"iso88591", "", line)
        line = re.sub(r"iso-?10646", "", line)
        line = re.sub(r"GB18030", "", line)
        line = re.sub(r"cns11643", "", line)
        line = re.sub(r"[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]*", "", line)

        if not re.search("(?<![gu#])[0-9][0-9][0-9][0-9][0-9]", line) and not re.search(" 1\.[0-9]", line):
            continue
    else:
        if not re.search('revno[ :][0-9][0-9.][0-9]', line) and not re.search('r[0-9][0-9.][0-9]', line) and not re.search('revision [0-9][0-9.][0-9]', line):
            continue
    sys.stdout.write(line)
