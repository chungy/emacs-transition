# Makefile to semi-automate the Emacs transition workflow.

# This is the official git mirror of the Bazaar repository
GITMIRROR = git.sv.gnu.org:/srv/git/emacs.git

# Bump for each trial release.
TRIAL=7

# Where to create the conversion
PUSHTO = git@gitorious.org:emacs-transition/review$(TRIAL).git
PUBLIC = git://gitorious.org/emacs-transition/review$(TRIAL).git

# You will have to change these
BZRMIRROR = ~/src/emacs/trunk

default:
	date 
	make --quiet emacs-git bzr-revisions COMMENT-DEFECTS CHANGELOG-DEFECTS 
	make --quiet bzr-revisions
	./firstpatch.sh
	date

# Clone the repo, then prune and rename branches.
# We use --bare, then hack the config file, in order
# to de-remote the remote branches.
mirror-git:
	git clone --bare $(GITMIRROR) scratch
	mkdir mirror-git
	mv scratch mirror-git/.git
	cp mirrorconfig mirror-git/.git/config
	(cd mirror-git >/dev/null; git checkout; ../branchprune.sh)

# Make a dump from the pruned repo.
mirror.fi: mirror-git
	(cd mirror-git >/dev/null; git fast-export --all .) >mirror.fi

# Write a dump that is truncated just past the point where the attic names
# were garbage-collected.  This loads much faster.
attic.fi: mirror.fi
	cyreposurgeon "read <mirror.fi" "1..<2001-07-06T09:41:18Z> write >attic.fi"

# Get a journal of all Changelog modifications up to the attic deletion 
# point, in a parseable form.  This can be used to find attic file deletion
# points.
changelogs.py: attic.fi
	cyreposurgeon "script changelog.rs attic.fi" >changelogs.py

# Get the Bazaar comment history
BZR.LOG:
	(cd $(BZRMIRROR); bzr up; bzr log --levels=0) >BZR.LOG

# Make a revision-to-action-stamp map of the Emacs mirror
bzr-revisions: BZR.LOG
	bzrlog -m <BZR.LOG >bzr-revisions

# Generate append inclusions from the Bazaar log
append.rs: BZR.LOG
	bzrlog -b <BZR.LOG >append.rs

# Edit the mirror repo according to the lift script
emacs.fi: mirror.fi emacs.lift append.rs
	cyreposurgeon "set canonicalize" "read <mirror.fi" "script emacs.lift" "prefer git" "write >emacs.fi"

# Make the converted repository
emacs-git: emacs.fi
	rm -fr emacs-git; git init emacs-git
	(cd emacs-git >/dev/null; git fast-import <../emacs.fi; git tag -d `git tag -l | grep empty`;)

# Push with mirroring to the conversion location. 
# Assumes the remote has been set.
firstpush: emacs-git
	(cd emacs-git >/dev/null; git gc --aggressive; git remote add origin $(PUSHTO); git push --mirror)

# Find ChangeLog Bazaar references in the checkout
CHANGELOG-DEFECTS:
	find emacs-git -type f -name '*ChangeLog*' -exec egrep '\([^a-z#=][^a-z#=][0-9][0-9][0-9][0-9][0-9]\)|1\.[0-9]' {} \; | logfilter.py >CHANGELOG-DEFECTS

# Find bzr references in the commit logs
COMMENT-DEFECTS:
	cd emacs-git >/dev/null; git checkout >/dev/null; git --no-pager log | ../logfilter.py >../COMMENT-DEFECTS

# Get a copy of the ancient CVS repo
emacs-cvs: 
	rsync -avz cvs.sv.gnu.org::sources/emacs/ emacs-cvs

# Extract the Aquamacs history from David Reitter's repo.
aquamacs-branch.fi:
	reposurgeon "read aquamacs-emacs" "@dsc(<2005-06-06T18:41:57Z!david.reitter@gmail.com>) write --noincremental --callout >aquamacs-branch.fi"

clean:
	rm -fr .rs*

nuke: clean
	rm -fr emacs.fi mirror.fi emacs-git mirror-git BZR.LOG bzr-revisions emacs-recloned typescript

# Use this to test-clone the conversion, to verify all branches are present 
reclone:
	git clone $(PUBLIC) emacs-recloned

# Generate a notification for the development list (after 'make firstpush').
# Pipe to sendmail -t -v
listmail:
	@announce.sh $(PUBLIC)
