#!/usr/bin/env python
#
# Filter a file doing replacements based on the mirror.map file

import sys

if __name__ == '__main__':
    replacements = []
    for line in open("mirror.map"):
        id, stamp = line.strip().split()
        replacements.append((id, stamp))
    for line in sys.stdin:
        for (id, stamp) in replacements:
            line = line.replace(" " + id + "\n", " " + stamp + "\n")
        sys.stdout.write(line)

