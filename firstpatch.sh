#!/bin/bash
#
# Apply the transition-day patch, also updating Changelog files
# Required because trying to patch ChangeLogs seldom ends well.
#
set -x

function prepend() {
	 cat $1 $2 >$2.new
	 mv $2.new $2
}

cat >>/tmp/ROOT_PREAMBLE <<EOF
2014-11-11  Eric S. Raymond  <esr@thyrsus.com>

	* Makefile.in: git transition - set VCWITNESS appropriately for git.

	All bzr revision IDS, and all CVS revision IDs for which a commit
	could be identified, were changed to time-date!committer version
	stamps. All .cvsignore files in the history became .gitignore
	files. Fixes-bug annotations from bzr were copied into the
	corresponding commit comments.

	(The first .cvsignore commit was 1999-09-30T14:07:54Z!fx@gnu.org.
	The last CVS commit was	2009-12-27T08:11:12Z!cyd@stupidchicken.com.)

	Committer/author email addresses are generally correct for the
	transition day, not necessarily when the comit was originally
	made.

EOF

cat >>/tmp/ADMIN_PREAMBLE <<EOF
2014-11-11  Eric S. Raymond  <esr@thyrsus.com>

	* make-tarball.txt, update-copyright, admin/notes/bugtracker,
	admin/notes/tags, admin/update-copyright, admin/update_autogen:
	admin/repo.notes: git transition.

EOF

cat >>/tmp/ETC_PREAMBLE <<EOF
2014-11-11  Eric S. Raymond  <esr@thyrsus.com>

	* CONTRIBUTE: git transition.

EOF

cat >>/tmp/EXPLANATION <<EOF
Git transition patch

All bzr revision IDS, and all CVS revision IDs for which a commit
could be identified, were changed to time-date!committer version
stamps. All .cvsignore files in the history became .gitignore files.
Fixes-bug annotations from bzr were copied into the corresponding
commit comments.

(The first .cvsignore commit was 1999-09-30T14:07:54Z!fx@gnu.org>.
The last CVS commit was <2009-12-27T08:11:12Z!cyd@stupidchicken.com>)

Committer/author email addresses are generally correct for the
transition day, not necessarily when the commit was originally made.
EOF

cd emacs-git

git checkout

patch -p1 <../transition.patch

prepend /tmp/ROOT_PREAMBLE ChangeLog
prepend /tmp/ADMIN_PREAMBLE admin/ChangeLog
prepend /tmp/ETC_PREAMBLE etc/ChangeLog

git commit -a -F /tmp/EXPLANATION

rm /tmp/ROOT_PREAMBLE /tmp/ETC_PREAMBLE /tmp/ADMIN_PREAMBLE /tmp/EXPLANATION

#end

