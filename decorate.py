#!/usr/bin/env python
#
# Use the mirror.map files to decorate the emacs.lift file

import sys, re, shlex

def deregexify(s):
    return s.replace(r'\(', '(').replace(r'\)', ')').replace(r'\.', '.')

if __name__ == '__main__':
    revmap = {}
    for fn in ["mirror.map"]:
        for line in open(fn):
            try:
                (revno, stamp) = line.strip().split()
            except ValueError:
                sys.stderr.write(line)
                sys.exit(1)
            revmap[revno] = stamp

    filtering = False
    keys = set([])
    revno_re = re.compile(r"(?<!1[.])[0-9][0-9][0-9][0-9][0-9][0-9.]*")
    checkable = ("changelog", "changecomment", "changeboth",
                 "change3log", "change3comment", "change3both",
                 "change4log", "change4comment", "change4both",
                 )
    for line in sys.stdin:
        if line.startswith("# BAZAAR"):
            filtering = True
        if line.startswith("# CVS"):
            filtering = False
        if not filtering or not line.startswith("do change"):
            sys.stdout.write(line)
            continue
        fields = shlex.split(line)
        assert fields[0] == "do" and fields[1] in checkable
        if (fields[1], fields[2]) in keys:
            continue
        keys.add((fields[1], fields[2]))
        m = revno_re.search(deregexify(fields[2]))
        if not m:
            sys.stderr.write("Couldn't extract a bzr revision from %s" % fields[2])
            sys.exit(1)
        revno = m.group(0)
        if len(fields) == 3:
            print 'do %s "%s" "%s"' % (fields[1], fields[2], revmap[revno])
        elif revmap[revno] in fields[3]:
            sys.stdout.write(line)
        else:
            sys.stderr.write("Mismatch on %s\n" % revno)
            sys.exit(1)

# end
