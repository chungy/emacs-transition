# Macros for reference-lifting.
#
# Factored out so we can conveniently include them for testing.

=B & [/ChangeLog/] & (/\b[0-9][0-9][0-9][0-9][0-9]/B | /\b1\.[0-9]/B) assign changelogs
=C & (/\br*[0-9][0-9][0-9][0-9][0-9]/c | /\b1\.[0-9]/c) assign changecomments

define changelog <changelogs> & (<{1}>..$) filter --regexp /{0}(?![0-9])/{1}/g
define changecomment <changecomments> & (<{1}>..$) filter --regexp /{0}(?![0-9])/{1}/g
define nochecklog <changelogs> & (<{1}>..$) filter --regexp /{0}(?![0-9])/{1}/g
define nocheckcomment <changecomments> & (<{1}>..$) filter --regexp /{0}(?![0-9])/{1}/cg

define change3log <changelogs> & (<{1}>..$) filter --regexp /{0}(?![0-9])/{2}{1}/g
define change3comment <changecomments> & (<{1}>..$) filter --regexp /{0}(?![0-9])/{2}{1}/cg

define change4log <changelogs> & (<{1}>..$) filter --regexp /{0}(?![0-9])/{1}{2}/g
define change4comment <changecomments> & (<{1}>..$) filter --regexp /{0}(?![0-9])/{1}{2}/cg

# These variants don't check that the target is findable
define everylog <changelogs> filter --regexp /{0}(?![0-9])/{1}/g
define everycomment <changecomments> filter --regexp /{0}(?![0-9])/{1}/cg

define changeboth {
do changelog "{0}" "{1}"
do changecomment "{0}" "{1}"
}
define nocheckboth {
do nochecklog "{0}" "{1}"
do nocheckcomment "{0}" "{1}"
}
define change3both {
do change3log "{0}" "{1}" "{2}"
do change3comment "{0}" "{1}" "{2}"
}
define change4both {
do change4log "{0}" "{1}" "{2}"
do change4comment "{0}" "{1}" "{2}"
}
define everyboth {
do everylog "{0}" "{1}"
do everycomment "{0}" "{1}"
}

# end
