#!/bin/bash
#
# Prune and rename various tags in the Emacs repository. 
#
# The point of doing this before reposurgeon runs is to simplify the branch
# structure so it's easier to see what commits belong to which actual branches,

function tagdelete {
git tag -d $1
}

function tagrename {
git tag $2 $1
git tag -d $1
}

function branchdelete {
git branch -D $1
}

tagdelete 0.1
tagdelete Boehm-GC-base
tagdelete CEDET_BASE
tagrename EMACS_19_34 emacs-19.34
tagrename EMACS_20_1 emacs-20.1
tagrename EMACS_20_2 emacs-20.2
tagrename EMACS_20_3 emacs-20.3
tagrename EMACS_20_4 emacs-20.4
tagrename EMACS_21_1 emacs-21.1
tagdelete EMACS_21_1_BASE
tagrename EMACS_21_2 emacs-21.2
tagrename EMACS_21_3 emacs-21.3
tagrename EMACS_22_1 emacs-22.1
tagrename EMACS_22_2 emacs-22.2
tagrename EMACS_22_3 emacs-22.3
tagdelete EMACS_22_BRANCHPOINT
tagrename EMACS_23_1 emacs-23.1
tagdelete EMACS_23_1_BASE
tagrename EMACS_23_2 emacs-23.2
tagrename EMACS_23_3 emacs-23.3
tagrename EMACS_23_4 emacs-23.4
tagrename EMACS_PRETEST_21_0_100 emacs-pretest-21.0.100
tagrename EMACS_PRETEST_21_0_101 emacs-pretest-21.0.101
tagrename EMACS_PRETEST_21_0_102 emacs-pretest-21.0.102
tagrename EMACS_PRETEST_21_0_103 emacs-pretest-21.0.103
tagrename EMACS_PRETEST_21_0_104 emacs-pretest-21.0.104
tagrename EMACS_PRETEST_21_0_105 emacs-pretest-21.0.105
tagrename EMACS_PRETEST_21_0_106 emacs-pretest-21.0.106
tagrename EMACS_PRETEST_21_0_90 emacs-pretest-21.0.90
tagrename EMACS_PRETEST_21_0_91 emacs-pretest-21.0.91
tagrename EMACS_PRETEST_21_0_92 emacs-pretest-21.0.92
tagrename EMACS_PRETEST_21_0_93 emacs-pretest-21.0.93
tagrename EMACS_PRETEST_21_0_95 emacs-pretest-21.0.95
tagrename EMACS_PRETEST_21_0_96 emacs-pretest-21.0.96
tagrename EMACS_PRETEST_21_0_97 emacs-pretest-21.0.97
tagrename EMACS_PRETEST_21_0_98 emacs-pretest-21.0.98
tagrename EMACS_PRETEST_21_0_99 emacs-pretest-21.0.99
tagrename EMACS_PRETEST_21_2_91 emacs-pretest-21.2.91
tagrename EMACS_PRETEST_21_2_92 emacs-pretest-21.2.92
tagrename EMACS_PRETEST_21_2_93 emacs-pretest-21.2.93
tagrename EMACS_PRETEST_21_2_94 emacs-pretest-21.2.94
tagrename EMACS_PRETEST_21_2_95 emacs-pretest-21.2.95
tagrename EMACS_PRETEST_22_0_90 emacs-pretest-22.0.90
tagrename EMACS_PRETEST_22_0_91 emacs-pretest-22.0.91
tagrename EMACS_PRETEST_22_0_92 emacs-pretest-22.0.92
tagrename EMACS_PRETEST_22_0_93 emacs-pretest-22.0.93
tagrename EMACS_PRETEST_22_0_94 emacs-pretest-22.0.94
tagrename EMACS_PRETEST_22_0_95 emacs-pretest-22.0.95
tagrename EMACS_PRETEST_22_0_96 emacs-pretest-22.0.96
tagrename EMACS_PRETEST_22_0_97 emacs-pretest-22.0.97
tagrename EMACS_PRETEST_22_0_98 emacs-pretest-22.0.98
tagrename EMACS_PRETEST_22_0_99 emacs-pretest-22.0.99
tagrename EMACS_PRETEST_22_0_990 emacs-pretest-22.0.990
tagrename EMACS_PRETEST_22_1_90 emacs-pretest-22.1.90
tagrename EMACS_PRETEST_22_1_91 emacs-pretest-22.1.91
tagrename EMACS_PRETEST_22_1_92 emacs-pretest-22.1.92
tagrename EMACS_PRETEST_22_2_90 emacs-pretest-22.2.90
tagrename EMACS_PRETEST_22_2_91 emacs-pretest-22.2.91
tagrename EMACS_PRETEST_22_2_92 emacs-pretest-22.2.92
tagrename EMACS_PRETEST_23_0_90 emacs-pretest-23.0.90
tagrename EMACS_PRETEST_23_0_91 emacs-pretest-23.0.91
tagrename EMACS_PRETEST_23_0_92 emacs-pretest-23.0.92
tagrename EMACS_PRETEST_23_0_93 emacs-pretest-23.0.93
tagrename EMACS_PRETEST_23_0_94 emacs-pretest-23.0.94
tagrename EMACS_PRETEST_23_0_95 emacs-pretest-23.0.95
tagrename EMACS_PRETEST_23_0_96 emacs-pretest-23.0.96
tagrename EMACS_PRETEST_23_1_90 emacs-pretest-23.1.90
tagrename EMACS_PRETEST_23_1_91 emacs-pretest-23.1.91
tagrename EMACS_PRETEST_23_1_92 emacs-pretest-23.1.92
tagrename EMACS_PRETEST_23_1_93 emacs-pretest-23.1.93
tagrename EMACS_PRETEST_23_1_94 emacs-pretest-23.1.94
tagrename EMACS_PRETEST_23_1_95 emacs-pretest-23.1.95
tagrename EMACS_PRETEST_23_1_96 emacs-pretest-23.1.96
tagrename EMACS_PRETEST_23_1_97 emacs-pretest-23.1.97
tagrename EMACS_PRETEST_23_2_90 emacs-pretest-23.2.90
tagrename EMACS_PRETEST_23_2_91 emacs-pretest-23.2.91
tagrename EMACS_PRETEST_23_2_92 emacs-pretest-23.2.92
tagrename EMACS_PRETEST_23_2_93 emacs-pretest-23.2.93
tagrename EMACS_PRETEST_23_2_93_1 emacs-pretest-23.2.93.1
tagrename EMACS_PRETEST_23_2_94 emacs-pretest-23.2.94
tagrename EMACS_PRETEST_23_3_90 emacs-pretest-23.3.90
tagrename EMACS_PRETEST_24_0_05 emacs-pretest-24.0.05
tagrename EMACS_PRETEST_24_0_90 emacs-pretest-24.0.90
tagrename EMACS_PRETEST_24_0_91 emacs-pretest-24.0.91
tagrename EMACS_PRETEST_24_0_92 emacs-pretest-24.0.92
tagrename EMACS_PRETEST_24_0_93 emacs-pretest-24.0.93
tagrename EMACS_PRETEST_24_0_94 emacs-pretest-24.0.94
tagrename EMACS_PRETEST_24_0_95 emacs-pretest-24.0.95
tagdelete Ilya_4_23
tagdelete Ilya_4_32
tagdelete Ilya_4_35
tagdelete Ilya_5_0
tagdelete Ilya_5_10
tagdelete Ilya_5_22
tagdelete Ilya_5_23
tagdelete Ilya_5_3
tagdelete Ilya_5_7
tagdelete NewVC-fileset-BASE
tagdelete RMAIL-MBOX-BASE
tagdelete Release_5_25
tagdelete URL_2004-04-04_01h16
tagdelete VENDOR-3_3
tagdelete VENDOR-3_4
tagdelete VENDOR-3_5
tagdelete VENDOR-3_6
tagdelete XFT_JHD_BRANCH_base
tagdelete after-merge-gnus-5_10
tagdelete amigados-merge
tagdelete before-merge-emacs-app-to-trunk
tagdelete before-merge-gnus-5_10
tagdelete before-merge-multi-tty-to-trunk
tagdelete before-merge-unicode-to-trunk
tagdelete before-miles-orphaned-changes
tagdelete before-remove-carbon
tagdelete before-remove-vms
tagdelete before-thomas-posix1996
tagdelete branchpoint-5_8
tagdelete custom_themes_branchpoint
# keep emacs-19.34
# keep emacs-20.1
# keep emacs-20.2
# keep emacs-20.3
# keep emacs-20.4
# keep emacs-21.1
# keep emacs-21.2
# keep emacs-21.3
# keep emacs-22.1
# keep emacs-22.2
# keep emacs-22.3
# keep emacs-23.1
# keep emacs-23.2
# keep emacs-23.3
# keep emacs-23.4
tagdelete emacs-23/emacs-23.2
# keep emacs-24.0.96
# keep emacs-24.0.97
# keep emacs-24.1
# keep emacs-24.2
# keep emacs-24.2.90
# keep emacs-24.2.91
# keep emacs-24.2.92
# keep emacs-24.2.93
# keep emacs-24.3
tagdelete emacs-24.3-base
# keep emacs-24.3-rc1
tagdelete emacs-bidi-base
tagdelete emacs-unicode-2-base
tagdelete emacs-unicode-2-pre-sync
tagdelete emacs-unicode-base
tagdelete font-backend-base
tagdelete gcc-2_8_1-980401
tagdelete gcc-2_8_1-980402
tagdelete gcc-2_8_1-980407
tagdelete gcc-2_8_1-980412
tagdelete gcc-2_8_1-980413
tagdelete gcc-2_8_1-980419
tagdelete gcc-2_8_1-980426
tagdelete gcc-2_8_1-980502
tagdelete gcc-2_8_1-980513
tagdelete gcc-2_8_1-980525
tagdelete gcc-2_8_1-980529
tagdelete gcc-2_8_1-980608
tagdelete gcc-2_8_1-980609
tagdelete gcc-2_8_1-980627
tagdelete gcc-2_8_1-980705
tagdelete gcc-2_8_1-980718
tagdelete gcc-2_8_1-980811
tagdelete gcc-2_8_1-980813
tagdelete gcc-2_8_1-980928
tagdelete gcc-2_8_1-980929
tagdelete gcc-2_8_1-RELEASE
tagdelete gcc_2_8_1-980315
tagdelete gcc_2_8_1-980929
tagdelete glibc-2_0_2
tagdelete glibc-2_0_4
tagdelete gnumach-release-1-1
tagdelete gnumach-release-1-1-1
tagdelete gnumach-release-1-1-2
tagdelete gnumach-release-1-1-3
tagdelete gnus-5_10-branchpoint
tagdelete gnus-5_10-post-merge-josefsson
tagdelete gnus-5_10-post-merge-yamaoka
tagdelete gnus-5_10-pre-merge-josefsson
tagdelete gnus-5_10-pre-merge-yamaoka
tagdelete handa-temp-tag
tagdelete hurd-release-0-2
tagdelete jimb-sync-Nov-3-1992
tagdelete kfs_20030524_post
tagdelete kfs_20030524_pre
##tagdelete lexbind-base
tagdelete libc-1-90
tagdelete libc-1-91
tagdelete libc-1-92
tagdelete libc-1-93
tagdelete libc-950402
tagdelete libc-950411
tagdelete libc-950722
tagdelete libc-950723
tagdelete libc-950922
tagdelete libc-951016
tagdelete libc-951018
tagdelete libc-951029
tagdelete libc-951031
tagdelete libc-951101
tagdelete libc-951102
tagdelete libc-951103
tagdelete libc-951104
tagdelete libc-951105
tagdelete libc-951106
tagdelete libc-951107
tagdelete libc-951108
tagdelete libc-951109
tagdelete libc-951110
tagdelete libc-951111
tagdelete libc-951112
tagdelete libc-951113
tagdelete libc-951114
tagdelete libc-951115
tagdelete libc-951116
tagdelete libc-951117
tagdelete libc-951118
tagdelete libc-951119
tagdelete libc-951120
tagdelete libc-951121
tagdelete libc-951122
tagdelete libc-951123
tagdelete libc-951124
tagdelete libc-951125
tagdelete libc-951126
tagdelete libc-951127
tagdelete libc-951128
tagdelete libc-951129
tagdelete libc-951130
tagdelete libc-951201
tagdelete libc-951202
tagdelete libc-951203
tagdelete libc-951204
tagdelete libc-951206
tagdelete libc-951208
tagdelete libc-951209
tagdelete libc-951210
tagdelete libc-951211
tagdelete libc-951212
tagdelete libc-951213
tagdelete libc-951214
tagdelete libc-951215
tagdelete libc-951216
tagdelete libc-951217
tagdelete libc-951218
tagdelete libc-951219
tagdelete libc-951220
tagdelete libc-951221
tagdelete libc-951222
tagdelete libc-951223
tagdelete libc-951224
tagdelete libc-951225
tagdelete libc-951226
tagdelete libc-951227
tagdelete libc-951228
tagdelete libc-951229
tagdelete libc-951230
tagdelete libc-951231
tagdelete libc-960101
tagdelete libc-960102
tagdelete libc-960103
tagdelete libc-960104
tagdelete libc-960105
tagdelete libc-960106
tagdelete libc-960107
tagdelete libc-960108
tagdelete libc-960109
tagdelete libc-960110
tagdelete libc-960111
tagdelete libc-960112
tagdelete libc-960113
tagdelete libc-960114
tagdelete libc-960115
tagdelete libc-960116
tagdelete libc-960117
tagdelete libc-960118
tagdelete libc-960119
tagdelete libc-960120
tagdelete libc-960121
tagdelete libc-960122
tagdelete libc-960123
tagdelete libc-960124
tagdelete libc-960125
tagdelete libc-960126
tagdelete libc-960127
tagdelete libc-960128
tagdelete libc-960129
tagdelete libc-960130
tagdelete libc-960131
tagdelete libc-960201
tagdelete libc-960202
tagdelete libc-960203
tagdelete libc-960204
tagdelete libc-960205
tagdelete libc-960206
tagdelete libc-960207
tagdelete libc-960208
tagdelete libc-960209
tagdelete libc-960210
tagdelete libc-960211
tagdelete libc-960212
tagdelete libc-960213
tagdelete libc-960214
tagdelete libc-960215
tagdelete libc-960216
tagdelete libc-960217
tagdelete libc-960218
tagdelete libc-960219
tagdelete libc-960220
tagdelete libc-960221
tagdelete libc-960222
tagdelete libc-960223
tagdelete libc-960224
tagdelete libc-960225
tagdelete libc-960226
tagdelete libc-960227
tagdelete libc-960228
tagdelete libc-960229
tagdelete libc-960302
tagdelete libc-960303
tagdelete libc-960304
tagdelete libc-960305
tagdelete libc-960306
tagdelete libc-960307
tagdelete libc-960308
tagdelete libc-960309
tagdelete libc-960310
tagdelete libc-960311
tagdelete libc-960312
tagdelete libc-960313
tagdelete libc-960314
tagdelete libc-960315
tagdelete libc-960316
tagdelete libc-960317
tagdelete libc-960318
tagdelete libc-960319
tagdelete libc-960320
tagdelete libc-960321
tagdelete libc-960322
tagdelete libc-960323
tagdelete libc-960324
tagdelete libc-960325
tagdelete libc-960326
tagdelete libc-960327
tagdelete libc-960328
tagdelete libc-960329
tagdelete libc-960330
tagdelete libc-960331
tagdelete libc-960401
tagdelete libc-960402
tagdelete libc-960403
tagdelete libc-960404
tagdelete libc-960405
tagdelete libc-960406
tagdelete libc-960407
tagdelete libc-960408
tagdelete libc-960409
tagdelete libc-960410
tagdelete libc-960411
tagdelete libc-960412
tagdelete libc-960413
tagdelete libc-960414
tagdelete libc-960415
tagdelete libc-960416
tagdelete libc-960417
tagdelete libc-960418
tagdelete libc-960419
tagdelete libc-960420
tagdelete libc-960421
tagdelete libc-960422
tagdelete libc-960423
tagdelete libc-960424
tagdelete libc-960425
tagdelete libc-960426
tagdelete libc-960427
tagdelete libc-960428
tagdelete libc-960429
tagdelete libc-960430
tagdelete libc-960501
tagdelete libc-960502
tagdelete libc-960503
tagdelete libc-960504
tagdelete libc-960505
tagdelete libc-960506
tagdelete libc-960507
tagdelete libc-960508
tagdelete libc-960509
tagdelete libc-960510
tagdelete libc-960511
tagdelete libc-960512
tagdelete libc-960513
tagdelete libc-960514
tagdelete libc-960515
tagdelete libc-960516
tagdelete libc-960517
tagdelete libc-960518
tagdelete libc-960519
tagdelete libc-960520
tagdelete libc-960521
tagdelete libc-960522
tagdelete libc-960523
tagdelete libc-960524
tagdelete libc-960525
tagdelete libc-960526
tagdelete libc-960527
tagdelete libc-960528
tagdelete libc-960529
tagdelete libc-960530
tagdelete libc-960531
tagdelete libc-960601
tagdelete libc-960602
tagdelete libc-960603
tagdelete libc-960604
tagdelete libc-960605
tagdelete libc-960606
tagdelete libc-960607
tagdelete libc-960608
tagdelete libc-960609
tagdelete libc-960610
tagdelete libc-960611
tagdelete libc-960612
tagdelete libc-960613
tagdelete libc-960614
tagdelete libc-960615
tagdelete libc-960616
tagdelete libc-960617
tagdelete libc-960618
tagdelete libc-960619
tagdelete libc-960620
tagdelete libc-960621
tagdelete libc-960622
tagdelete libc-960623
tagdelete libc-960624
tagdelete libc-960625
tagdelete libc-960626
tagdelete libc-960627
tagdelete libc-960628
tagdelete libc-960629
tagdelete libc-960630
tagdelete libc-960701
tagdelete libc-960702
tagdelete libc-960703
tagdelete libc-960704
tagdelete libc-960705
tagdelete libc-960706
tagdelete libc-960707
tagdelete libc-960708
tagdelete libc-960709
tagdelete libc-960710
tagdelete libc-960711
tagdelete libc-960712
tagdelete libc-960713
tagdelete libc-960714
tagdelete libc-960715
tagdelete libc-960716
tagdelete libc-960717
tagdelete libc-960718
tagdelete libc-960719
tagdelete libc-960720
tagdelete libc-960721
tagdelete libc-960722
tagdelete libc-960723
tagdelete libc-960724
tagdelete libc-960725
tagdelete libc-960726
tagdelete libc-960727
tagdelete libc-960728
tagdelete libc-960729
tagdelete libc-960730
tagdelete libc-960731
tagdelete libc-960801
tagdelete libc-960802
tagdelete libc-960803
tagdelete libc-960804
tagdelete libc-960805
tagdelete libc-960806
tagdelete libc-960807
tagdelete libc-960808
tagdelete libc-960809
tagdelete libc-960810
tagdelete libc-960811
tagdelete libc-960812
tagdelete libc-960813
tagdelete libc-960814
tagdelete libc-960815
tagdelete libc-960816
tagdelete libc-960817
tagdelete libc-960818
tagdelete libc-960819
tagdelete libc-960820
tagdelete libc-960821
tagdelete libc-960822
tagdelete libc-960823
tagdelete libc-960824
tagdelete libc-960825
tagdelete libc-960826
tagdelete libc-960827
tagdelete libc-960828
tagdelete libc-960829
tagdelete libc-960830
tagdelete libc-960831
tagdelete libc-960901
tagdelete libc-960902
tagdelete libc-960903
tagdelete libc-960904
tagdelete libc-960905
tagdelete libc-960906
tagdelete libc-960907
tagdelete libc-960908
tagdelete libc-960909
tagdelete libc-960910
tagdelete libc-960911
tagdelete libc-960912
tagdelete libc-960913
tagdelete libc-960918
tagdelete libc-960919
tagdelete libc-960920
tagdelete libc-960921
tagdelete libc-960922
tagdelete libc-960923
tagdelete libc-960925
tagdelete libc-960926
tagdelete libc-960927
tagdelete libc-960928
tagdelete libc-960929
tagdelete libc-961001
tagdelete libc-961004
tagdelete libc-961005
tagdelete libc-961006
tagdelete libc-961007
tagdelete libc-961008
tagdelete libc-961009
tagdelete libc-961010
tagdelete libc-961011
tagdelete libc-961012
tagdelete libc-961013
tagdelete libc-961014
tagdelete libc-961015
tagdelete libc-961016
tagdelete libc-961017
tagdelete libc-961018
tagdelete libc-961019
tagdelete libc-961020
tagdelete libc-961021
tagdelete libc-961022
tagdelete libc-961023
tagdelete libc-961024
tagdelete libc-961025
tagdelete libc-961026
tagdelete libc-961027
tagdelete libc-961028
tagdelete libc-961029
tagdelete libc-961030
tagdelete libc-961031
tagdelete libc-961101
tagdelete libc-961102
tagdelete libc-961103
tagdelete libc-961104
tagdelete libc-961105
tagdelete libc-961106
tagdelete libc-961107
tagdelete libc-961108
tagdelete libc-961109
tagdelete libc-961110
tagdelete libc-961111
tagdelete libc-961114
tagdelete libc-961115
tagdelete libc-961116
tagdelete libc-961117
tagdelete libc-961118
tagdelete libc-961119
tagdelete libc-961120
tagdelete libc-961121
tagdelete libc-961203
tagdelete libc-961204
tagdelete libc-961205
tagdelete libc-961206
tagdelete libc-961207
tagdelete libc-961208
tagdelete libc-961209
tagdelete libc-961210
tagdelete libc-961211
tagdelete libc-961212
tagdelete libc-961213
tagdelete libc-961214
tagdelete libc-961215
tagdelete libc-961216
tagdelete libc-961217
tagdelete libc-961218
tagdelete libc-961219
tagdelete libc-961220
tagdelete libc-961221
tagdelete libc-961222
tagdelete libc-961223
tagdelete libc-961224
tagdelete libc-961225
tagdelete libc-961226
tagdelete libc-961227
tagdelete libc-961228
tagdelete libc-961229
tagdelete libc-961230
tagdelete libc-961231
tagdelete libc-970101
tagdelete libc-970102
tagdelete libc-970103
tagdelete libc-970104
tagdelete libc-970105
tagdelete libc-970106
tagdelete libc-970107
tagdelete libc-970108
tagdelete libc-970109
tagdelete libc-970110
tagdelete libc-970111
tagdelete libc-970112
tagdelete libc-970113
tagdelete libc-970114
tagdelete libc-970115
tagdelete libc-970116
tagdelete libc-970117
tagdelete libc-970118
tagdelete libc-970119
tagdelete libc-970120
tagdelete libc-970121
tagdelete libc-970122
tagdelete libc-970123
tagdelete libc-970124
tagdelete libc-970125
tagdelete libc-970126
tagdelete libc-970127
tagdelete libc-970128
tagdelete libc-970129
tagdelete libc-970130
tagdelete libc-970131
tagdelete libc-970201
tagdelete libc-970202
tagdelete libc-970203
tagdelete libc-970204
tagdelete libc-970205
tagdelete libc-970206
tagdelete libc-970207
tagdelete libc-970208
tagdelete libc-970209
tagdelete libc-970210
tagdelete libc-970211
tagdelete libc-970212
tagdelete libc-970213
tagdelete libc-970214
tagdelete libc-970215
tagdelete libc-970216
tagdelete libc-970217
tagdelete libc-970218
tagdelete libc-970219
tagdelete libc-970220
tagdelete libc-970221
tagdelete libc-970222
tagdelete libc-970223
tagdelete libc-970224
tagdelete libc-970225
tagdelete libc-970226
tagdelete libc-970227
tagdelete libc-970228
tagdelete libc-970301
tagdelete libc-970302
tagdelete libc-970303
tagdelete libc-970304
tagdelete libc-970305
tagdelete libc-970306
tagdelete libc-970307
tagdelete libc-970308
tagdelete libc-970309
tagdelete libc-970310
tagdelete libc-970311
tagdelete libc-970312
tagdelete libc-970313
tagdelete libc-970314
tagdelete libc-970315
tagdelete libc-970316
tagdelete libc-970317
tagdelete libc-970318
tagdelete libc-970319
tagdelete libc-970320
tagdelete libc-970321
tagdelete libc-970322
tagdelete libc-970323
tagdelete libc-970324
tagdelete libc-970325
tagdelete libc-970326
tagdelete libc-970327
tagdelete libc-970328
tagdelete libc-970329
tagdelete libc-970330
tagdelete libc-970331
tagdelete libc-970401
tagdelete libc-970402
tagdelete libc-970403
tagdelete libc-970404
tagdelete libc-970405
tagdelete libc-970406
tagdelete libc-970407
tagdelete libc-970408
tagdelete libc-970409
tagdelete libc-970410
tagdelete libc-970411
tagdelete libc-970412
tagdelete libc-970413
tagdelete libc-970414
tagdelete libc-970415
tagdelete libc-970416
tagdelete libc-970417
tagdelete libc-970418
tagdelete libc-970419
tagdelete libc-970420
tagdelete libc-970421
tagdelete libc-970422
tagdelete libc-970423
tagdelete libc-970424
tagdelete libc-970425
tagdelete libc-970426
tagdelete libc-970427
tagdelete libc-970428
tagdelete libc-970429
tagdelete libc-970430
tagdelete libc-970501
tagdelete libc-970502
tagdelete libc-970503
tagdelete libc-970504
tagdelete libc-970505
tagdelete libc-970506
tagdelete libc-970507
tagdelete libc-970508
tagdelete libc-970509
tagdelete libc-970510
tagdelete libc-970511
tagdelete libc-970512
tagdelete libc-970513
tagdelete libc-970514
tagdelete libc-970515
tagdelete libc-970516
tagdelete libc-970517
tagdelete libc-970518
tagdelete libc-970519
tagdelete libc-970520
tagdelete libc-970521
tagdelete libc-970522
tagdelete libc-970523
tagdelete libc-970524
tagdelete libc-970525
tagdelete libc-970526
tagdelete libc-970527
tagdelete libc-970528
tagdelete libc-970529
tagdelete libc-970530
tagdelete libc-970531
tagdelete libc-970601
tagdelete libc-970602
tagdelete libc-970603
tagdelete libc-970604
tagdelete libc-970605
tagdelete libc-970606
tagdelete libc-970607
tagdelete libc-970608
tagdelete libc-970609
tagdelete libc-970610
tagdelete libc-970611
tagdelete libc-970612
tagdelete libc-970613
tagdelete libc-970614
tagdelete libc-970615
tagdelete libc-970616
tagdelete libc-970617
tagdelete libc-970618
tagdelete libc-970619
tagdelete libc-970620
tagdelete libc-970621
tagdelete libc-970622
tagdelete libc-970624
tagdelete libc-970625
tagdelete libc-970626
tagdelete libc-970627
tagdelete libc-970628
tagdelete libc-970629
tagdelete libc-970630
tagdelete libc-970701
tagdelete libc-970702
tagdelete libc-970703
tagdelete libc-970704
tagdelete libc-970705
tagdelete libc-970707
tagdelete libc-970708
tagdelete libc-970709
tagdelete libc-970710
tagdelete libc-970713
tagdelete libc-970715
tagdelete libc-970717
tagdelete libc-970718
tagdelete libc-970719
tagdelete libc-970720
tagdelete libc-970721
tagdelete libc-970722
tagdelete libc-970723
tagdelete libc-970724
tagdelete libc-970725
tagdelete libc-970726
tagdelete libc-970727
tagdelete libc-970728
tagdelete libc-970729
tagdelete libc-970730
tagdelete libc-970731
tagdelete libc-970801
tagdelete libc-970802
tagdelete libc-970803
tagdelete libc-970804
tagdelete libc-970805
tagdelete libc-970806
tagdelete libc-970807
tagdelete libc-970808
tagdelete libc-970809
tagdelete libc-970810
tagdelete libc-970811
tagdelete libc-970812
tagdelete libc-970813
tagdelete libc-970814
tagdelete libc-970815
tagdelete libc-970816
tagdelete libc-970817
tagdelete libc-970818
tagdelete libc-970819
tagdelete libc-970820
tagdelete libc-970821
tagdelete libc-970822
tagdelete libc-970823
tagdelete libc-970824
tagdelete libc-970825
tagdelete libc-970826
tagdelete libc-970827
tagdelete libc-970828
tagdelete libc-970829
tagdelete libc-970830
tagdelete libc-970831
tagdelete libc-970901
tagdelete libc-970902
tagdelete libc-970903
tagdelete libc-970904
tagdelete libc-970905
tagdelete libc-970906
tagdelete libc-970907
tagdelete libc-970908
tagdelete libc-970911
tagdelete libc-970912
tagdelete libc-970913
tagdelete libc-970914
tagdelete libc-970915
tagdelete libc-970916
tagdelete libc-970917
tagdelete libc-970918
tagdelete libc-970919
tagdelete libc-970920
tagdelete libc-970921
tagdelete libc-970922
tagdelete libc-970923
tagdelete libc-970924
tagdelete libc-970925
tagdelete libc-970926
tagdelete libc-970927
tagdelete libc-970928
tagdelete libc-970929
tagdelete libc-970930
tagdelete libc-971001
tagdelete libc-971018
tagdelete libc-971019
tagdelete libc-971020
tagdelete libc-971021
tagdelete libc-971022
tagdelete libc-971023
tagdelete libc-971024
tagdelete libc-971025
tagdelete libc-971026
tagdelete libc-971027
tagdelete libc-971028
tagdelete libc-971029
tagdelete libc-971030
tagdelete libc-971031
tagdelete libc-971101
tagdelete libc-971102
tagdelete libc-971103
tagdelete libc-971104
tagdelete libc-971105
tagdelete libc-971106
tagdelete libc-971107
tagdelete libc-971108
tagdelete libc-971109
tagdelete libc-971110
tagdelete libc-971111
tagdelete libc-971112
tagdelete libc-971113
tagdelete libc-971114
tagdelete libc-971115
tagdelete libc-971116
tagdelete libc-971117
tagdelete libc-971118
tagdelete libc-971120
tagdelete libc-971121
tagdelete libc-971122
tagdelete libc-971123
tagdelete libc-971124
tagdelete libc-971125
tagdelete libc-971126
tagdelete libc-971127
tagdelete libc-971128
tagdelete libc-971129
tagdelete libc-971130
tagdelete libc-971201
tagdelete libc-971203
tagdelete libc-971204
tagdelete libc-971205
tagdelete libc-971206
tagdelete libc-971207
tagdelete libc-971208
tagdelete libc-971209
tagdelete libc-971210
tagdelete libc-971211
tagdelete libc-971212
tagdelete libc-971213
tagdelete libc-971214
tagdelete libc-971217
tagdelete libc-971218
tagdelete libc-971219
tagdelete libc-971220
tagdelete libc-971221
tagdelete libc-971222
tagdelete libc-971223
tagdelete libc-971224
tagdelete libc-971225
tagdelete libc-971226
tagdelete libc-971227
tagdelete libc-971228
tagdelete libc-971229
tagdelete libc-971230
tagdelete libc-971231
tagdelete libc-980103
tagdelete libc-980104
tagdelete libc-980105
tagdelete libc-980106
tagdelete libc-980107
tagdelete libc-980108
tagdelete libc-980109
tagdelete libc-980110
tagdelete libc-980111
tagdelete libc-980112
tagdelete libc-980114
tagdelete libc-980115
tagdelete libc-980116
tagdelete libc-980117
tagdelete libc-980118
tagdelete libc-980119
tagdelete libc-980120
tagdelete libc-980121
tagdelete libc-980122
tagdelete libc-980123
tagdelete libc-980124
tagdelete libc-980125
tagdelete libc-980126
tagdelete libc-980127
tagdelete libc-980128
tagdelete libc-980129
tagdelete libc-980130
tagdelete libc-980212
tagdelete libc-980213
tagdelete libc-980214
tagdelete libc-980215
tagdelete libc-980216
tagdelete libc-980217
tagdelete libc-980218
tagdelete libc-980219
tagdelete libc-980220
tagdelete libc-980221
tagdelete libc-980222
tagdelete libc-980223
tagdelete libc-980224
tagdelete libc-980225
tagdelete libc-980226
tagdelete libc-980227
tagdelete libc-980228
tagdelete libc-980301
tagdelete libc-980302
tagdelete libc-980303
tagdelete libc-980304
tagdelete libc-980306
tagdelete libc-980307
tagdelete libc-980308
tagdelete libc-980309
tagdelete libc-980310
tagdelete libc-980311
tagdelete libc-980312
tagdelete libc-980313
tagdelete libc-980314
tagdelete libc-980315
tagdelete libc-980316
tagdelete libc-980317
tagdelete libc-980318
tagdelete libc-980319
tagdelete libc-980320
tagdelete libc-980321
tagdelete libc-980322
tagdelete libc-980323
tagdelete libc-980324
tagdelete libc-980325
tagdelete libc-980326
tagdelete libc-980327
tagdelete libc-980328
tagdelete libc-980329
tagdelete libc-980330
tagdelete libc-980331
tagdelete libc-980401
tagdelete libc-980402
tagdelete libc-980403
tagdelete libc-980404
tagdelete libc-980405
tagdelete libc-980406
tagdelete libc-980407
tagdelete libc-980408
tagdelete libc-980409
tagdelete libc-980410
tagdelete libc-980411
tagdelete libc-980412
tagdelete libc-980413
tagdelete libc-980414
tagdelete libc-980428
tagdelete libc-980429
tagdelete libc-980430
tagdelete libc-980501
tagdelete libc-980502
tagdelete libc-980503
tagdelete libc-980504
tagdelete libc-980505
tagdelete libc-980506
tagdelete libc-980507
tagdelete libc-980508
tagdelete libc-980509
tagdelete libc-980510
tagdelete libc-980512
tagdelete libc-980513
tagdelete libc-980514
tagdelete libc-980515
tagdelete libc-980516
tagdelete libc-980517
tagdelete libc-980518
tagdelete libc-980519
tagdelete libc-980520
tagdelete libc-980521
tagdelete libc-980522
tagdelete libc-980523
tagdelete libc-980524
tagdelete libc-980525
tagdelete libc-980526
tagdelete libc-980527
tagdelete libc-980528
tagdelete libc-980529
tagdelete libc-980530
tagdelete libc-980531
tagdelete libc-980601
tagdelete libc-980602
tagdelete libc-980603
tagdelete libc-980604
tagdelete libc-980605
tagdelete libc-980606
tagdelete libc-980607
tagdelete libc-980608
tagdelete libc-980609
tagdelete libc-980610
tagdelete libc-980611
tagdelete libc-980612
tagdelete libc-980613
tagdelete libc-980614
tagdelete libc-980615
tagdelete libc-980616
tagdelete libc-980617
tagdelete libc-980618
tagdelete libc-980619
tagdelete libc-980620
tagdelete libc-980621
tagdelete libc-980622
tagdelete libc-980623
tagdelete libc-980624
tagdelete libc-980625
tagdelete libc-980626
tagdelete libc-980627
tagdelete libc-980628
tagdelete libc-980629
tagdelete libc-980630
tagdelete libc-980701
tagdelete libc-980702
tagdelete libc-980703
tagdelete libc-980704
tagdelete libc-980705
tagdelete libc-980706
tagdelete libc-980707
tagdelete libc-980708
tagdelete libc-980709
tagdelete libc-980710
tagdelete libc-980711
tagdelete libc-980712
tagdelete libc-980713
tagdelete libc-980714
tagdelete libc-980715
tagdelete libc-980716
tagdelete libc-980717
tagdelete libc-980718
tagdelete libc-980719
tagdelete libc-980720
tagdelete libc20x-970306
tagdelete libc20x-97031
tagdelete libc20x-970316
tagdelete libc20x-970318
tagdelete libc20x-970319
tagdelete libc20x-970404
tagdelete libc20x-970417
tagdelete libc_1_09
tagdelete lisp-bob
tagdelete make-3-72-10
tagdelete make-3-72-11
tagdelete make-3-72-12
tagdelete make-3-72-13
tagdelete make-3-72-9
tagdelete make-3-73
tagdelete make-3-73-1
tagdelete make-3-73-2
tagdelete make-3-73-3
tagdelete make-3-74
tagdelete make-3-74-1
tagdelete make-3-74-2
tagdelete make-3-74-3
tagdelete make-3-74-4
tagdelete make-3-74-5
tagdelete make-3-74-6
tagdelete make-3-74-7
tagdelete make-3-75
tagdelete make-3-75-1
tagdelete make-3-75-91
tagdelete make-3-75-92
tagdelete make-3-75-93
tagdelete make-3-76
tagdelete make-3-76-1
tagdelete make-3-77
tagdelete merge-multi-tty-to-trunk
tagdelete merge-unicode-to-trunk
tagdelete mh-e-7_85
tagdelete mh-e-7_90
tagdelete mh-e-7_91
tagdelete mh-e-7_92
tagdelete mh-e-7_93
tagdelete mh-e-7_94
tagdelete mh-e-7_95
tagdelete mh-e-8.2.90
tagdelete mh-e-8.2.91
tagdelete mh-e-8.2.92
tagdelete mh-e-8.2.93
tagdelete mh-e-8.3
tagdelete mh-e-8.3.1
tagdelete mh-e-8.4
tagdelete mh-e-8.5
tagdelete mh-e-8_0
tagdelete mh-e-8_0_1
tagdelete mh-e-8_0_2
tagdelete mh-e-8_0_3
tagdelete mh-e-8_1
tagdelete mh-e-8_2
tagdelete mh-e-doc-7_94
tagdelete mh-e-doc-8.3
tagdelete mh-e-doc-8.4
tagdelete mh-e-doc-8.5
tagdelete mh-e-doc-8_0
tagdelete mh-e-doc-8_0_1
tagdelete mh-e-doc-8_0_3
tagdelete mh-e-doc-8_1
tagdelete mh-e-doc-8_2
tagdelete multi-tty-base
tagdelete patches_21_0_base
tagdelete raeburn-tag-4-for-export
tagdelete raeburn-tag-5-for-export
tagdelete raeburn-tag-6-for-export
tagdelete raeburn-tag-7-for-export
tagdelete release-0-0
tagdelete release-0-1
tagdelete release-1-0
tagdelete remove-carbon
tagdelete remove-vms
tagdelete root-libc-2_0_x-branch
tagdelete small-dump-base
tagdelete sml-mode-6.0
tagdelete sml-mode-6.1
tagdelete tmp_pcl_tag_131034C
# keep ttn-vms-21-2-B2
# keep ttn-vms-21-2-B3
# keep ttn-vms-21-2-B4
tagdelete unicode-post-font-backend
tagdelete unicode-pre-font-backend
tagdelete unicode-xft-base
tagdelete v0.1
tagdelete v0.1.0
tagdelete v0.1.1
tagdelete v0.1.2
tagdelete v0.1.3
tagdelete v0.2.0
tagdelete v0.3.0
tagdelete v1_7i
tagdelete v2007-Sep-10
tagdelete xwidget-0.2
tagdelete zsh-merge-ognus-1
tagdelete zsh-sync-ognus-2
tagdelete zsh-sync-ognus-3
tagdelete fx-branch-base
tagdelete lexbind-base
tagdelete lost+found/12d398afa4d2b77d606b28fe43a2e5200c863419
tagdelete lost+found/5cf718038f760d6dc2e21dee3ce98061a975488a
tagdelete lost+found/9a1501510c2a842e8597d695fec05563161a84b7
tagdelete lost+found/d4cd8d7c34cc79dc4abd0fec33cec74afa33e2d0
tagdelete lost+found/e387e14faeea08877c99ddf798d5a4c9f7999c72
tagdelete lost+found/b2e45f822e55882dda1bb80ca8b845d062d6d523
tagdelete lost+found/c003b5d7e29f2b7c3c340d4aa851a61cfa481ec8

# Keep old branches, except for elpa

# branchdelete old-branches/EMACS_21_1_RC
# branchdelete old-branches/EMACS_22_BASE
# branchdelete old-branches/EMACS_23_1_RC
# branchdelete old-branches/NewVC-fileset
# branchdelete old-branches/branch-5_8
# branchdelete old-branches/cedet-branch
branchdelete old-branches/elpa
# branchdelete old-branches/emacs-unicode
# branchdelete old-branches/emacs-unicode-2
# branchdelete old-branches/font-backend
# branchdelete old-branches/gerd_defvaralias
# branchdelete old-branches/gnus-5_10-branch
# branchdelete old-branches/imagemagick
# branchdelete old-branches/lexbind
# branchdelete old-branches/lexbind-new
# branchdelete old-branches/multi-tty
# branchdelete old-branches/pending
# branchdelete old-branches/profiler
# branchdelete old-branches/python
# branchdelete old-branches/rmail-mbox-branch
# branchdelete old-branches/unicode-xft
# branchdelete old-branches/window-pub
# branchdelete other-branches/Boehm-GC
# branchdelete other-branches/Boehm-versions
# branchdelete other-branches/DAVELOVE
# branchdelete other-branches/FLYSPELL
# branchdelete other-branches/ILYA
# branchdelete other-branches/URL
# branchdelete other-branches/VENDOR
branchdelete other-branches/XFT_JHD_BRANCH
# branchdelete other-branches/custom_themes
# branchdelete other-branches/fx-branch
# branchdelete other-branches/gerd_0001
# branchdelete other-branches/gerd_big
# branchdelete other-branches/gerd_dbe
# branchdelete other-branches/gerd_int
# branchdelete other-branches/glibc-2_0_x
branchdelete other-branches/master-UNNAMED-BRANCH
# branchdelete other-branches/miles-orphaned-changes
# branchdelete other-branches/old-bidi
# branchdelete other-branches/old-concurrency
# branchdelete other-branches/patches_21_0
# branchdelete other-branches/test2
# branchdelete other-branches/thomas-posix1996
# branchdelete other-branches/ttn-vms-21-2-stash
# branchdelete other-branches/ttn-vms-21-3-stash

# Delete by Bill Wohler's request
branchdelete mh-e
