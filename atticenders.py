#!/usr/bin/env python

import os,re, changelogs

atticfiles = (
    ("etc/", "MACHINES"),
    ("etc/", "news.texi"),
    ("etc/", "TO-DO"),
    ("lib-src/", "aixcc.lex <1995-04-04T21:11:29Z>"),
    ("lib-src/", "etags-vmslib.c"),
    ("lib-src/", "rcs2log"),
    ("lib-src/", "timer.c"),
    ("lib-src/", "wakeup.c"),
    ("lisp/", "ada.el"),
    ("lisp/", "batmode.el"),
    ("lisp/", "bytecpat.el"),
    ("lisp/", "cl.el"),
    ("lisp/", "cmulisp.el"),
    ("lisp/", "custom.el"),
    ("lisp/", "diary-ins.el"),
    ("lisp/", "diary-lib.el"),
    ("lisp/", "ftp.el"),
    ("lisp/", "gnus.el"),
    ("lisp/", "gnusmail.el"),
    ("lisp/gnus/", "md5.el"),
    ("lisp/", "gnusmisc.el"),
    ("lisp/gnus/", "nnheaderxm.el"),
    ("lisp/", "gnuspost.el"),
    ("lisp/", "gnus-uu.el"),
    ("lisp/", "gosmacs.el"),
    ("lisp/", "grow-vers.el"),
    ("lisp/", "inc-vers.el"),
    ("lisp/", "isearch-old.el"),
    ("lisp/", "iso8859-1.el"),
    ("lisp/", "libc.el"),
    ("lisp/", "man.el"),
    ("lisp/", "medit.el"),
    ("lisp/", "mh-e.el"),
    ("lisp/", "mhspool.el"),
    ("lisp/", "mim-mode.el"),
    ("lisp/", "mim-syntax.el"),
    ("lisp/", "netunam.el"),
    ("lisp/", "nnspool.el"),
    ("lisp/", "nntp.el"),
    ("lisp/", "old-shell.el"),
    ("lispref/", "buffer-local.texi"),
    ("lisp/", "sc-alist.el"),
    ("lisp/", "sc.el"),
    ("lisp/", "sc-elec.el"),
    ("lisp/", "setaddr.el"),
    ("lisp/", "speedbspec.el <1998-07-10T16:46:21Z>"),
    ("lisp/", "sun-keys.el"),
    ("lisp/", "superyank.el"),
    ("lisp/", "term-nasty.el"),
    ("lisp/textmodes/", "ispell4.el"),
    ("lisp/", "timer.el"),
    ("lisp/", "tpu-doc.el"),
    ("lisp/", "vmsx.el"),
    ("lisp/", "word-help.el"),
    ("packages/yasnippet/snippets/ruby-mode/general/", "b"),
    ("", "PROBLEMS"),
    ("src/", "convexos.h"),
    ("src/", "environ.c"),
    ("src/", "mach2.h"),
    ("src/", "Makefile.in"),
    ("src/m/", "dos386.h"),
    ("src/", "old-ralloc.c"),
    ("src/", "sol2-2.h"),
    ("src/", "unexelf1.c"),
    ("src/", "unexsgi.c"),
    ("src/", "x11term.h"),
    ("src/", "xscrollbar.h"),
    ("src/", "xselect.c.old"),
    ("src/", "XTests.c"),
    ("src/", "XTests.h"),
    )

# How many trailing entries do we want to see?
TAILSIZE = 2

if __name__ == '__main__':
    for (directory, base) in atticfiles:
        matches = []
        for (stamp, path, branch, comment) in changelogs.changelogs:
            if directory in path and re.search(r"\b%s\b" % base, comment):
                matches.append((stamp, path, branch, comment))
        if matches:
            print("*** %s%s:" % (directory, base))
            for (stamp, path, branch, comment) in matches[-TAILSIZE:]:
                print('Commit: %s\nPath: %s\nBranch: %s\n%s' %
                          (stamp, path, branch, comment))
