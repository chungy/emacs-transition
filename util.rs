# Check to see if a path exists in both attic and non-attic versions.
define check {
[/{0}={1}/] resolve Attic
[/{0}{1}/] & ~[/=/] resolve Non-attic
}

# Look for the last ChangeLog commit with a blob containing a specified string
# {0} and run the specified command {1}.
define lastchange {
@min(=B & [/ChangeLog$/] & /{0}/B)? {1}
}

# Visualize the repo.
define see {
graph >/tmp/repograph
!dot -Tsvg </tmp/repograph | display -
}
