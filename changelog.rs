# This extension reports ChangeLog modifications as a Python initializer
# The argument should be a segment of a stream dump of the Emacs repository.
# The output may need point deletions to cope with non-ASCII characters

read <$1
exec <<EOF
def changelogs(repository):
    print("changelogs = (")
    for commit in repository.commits():
	for fileop in commit.fileops:
	    if fileop.op == 'M':
                if fileop.path.endswith("ChangeLog"):
                    content = repository.objfind(fileop.ref).get_content()
                    state = 0
                    for end in range(len(content)):
                        if content[end] == '\n' and ((end >= len(content)-1) or not content[end+1].isspace()):
                            state += 1
                        if state > 0:
                            break
                    content = content[:end]
                    print('("%s",\n"%s",\n"%s",\nr"""%s\n"""),' %
                          (commit.action_stamp(), fileop.path, commit.branch, content))
    print(")")
EOF
1,$ eval changelogs(_repository)
# The following sets edit modes for GNU EMACS
# Local Variables:
# mode:python
# End:
# end
