#!/usr/bin/env python

# Run through the putput of bzr log looking for revision numbers.

import sys, re

state = 0
dump = False
for line in sys.stdin:
    if line.startswith("commit"):
        state = 0
        #if dump:
        #    sys.stdout.write(saved)
        dump = False
        saved = line
    elif line.startswith("Author") or line.startswith("Date"):
        saved += line
    elif line == "\n":
        state = 1
        saved += line
    elif state == 1:
        saved += line
        check = line
        check = check.replace("POSIX 1003.1-1988", "")
        check = check.replace("gnus--rel--5.10", "")
        check = check.replace("gnus--5.10", "")
        check = check.replace("gnus--rel--5.9", "")
        check = check.replace("gnus--5.9", "")
        check = check.replace("gnus--rel--5.8", "")
        check = check.replace("gnus--5.8", "")
        check = re.sub("http://lists.gnu.org/.*\.html", "", check)
        check = re.sub("http://debbugs.gnu.org/.*\.html", "", check)
        check = re.sub("http://debbugs.gnu.org/[#0-9]*", "", check)
        check = re.sub("[Vv]ersion[ .0-9-]*", "", check)
        check = re.sub(r"[Vv]ersion number\([ .0-9-]*", "", check)
        check = re.sub("[Rr]elease[ .0-9-]*", "", check)
        check = re.sub("GCC[ .0-9]*", "", check)
        check = re.sub("After[ .,0-9]*", "", check)
        check = re.sub("[Oo]rg[ .0-9]*", "", check)
        check = re.sub("[Ee]macs[ .0-9-]*", "", check)
        check = re.sub("Tramp[ .0-9]*", "", check)
        check = re.sub("GFDL[ .0-9]*", "", check)
        check = re.sub("MinGW[ .0-9]*", "", check)
        check = re.sub("GnuPG[ .0-9]*", "", check)
        check = re.sub("Synched with[ .0-9]*", "", check)
        check = re.sub("signed-bad[ .0-9-]*", "", check)
        check = re.sub("signed-good[ .0-9-]*", "", check)
        check = re.sub("OSX >= [ .0-9]*", "", check)
        check = re.sub("OS *X[ .0-9]*", "", check)
        check = re.sub("version to[ .0-9]*", "", check)
        check = re.sub("debbugs:[0-9]+", "", check)
        check = re.sub("[Bb]ugs* *#*[0-9]+", "", check)
        check = re.sub("[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]", "", check)
        check = re.sub(r"24\.[ .0-9]*", "", check)
        check = re.sub(r"23\.[ .0-9]*", "", check)
        check = re.sub(r"22\.[ .0-9]*", "", check)
        check = re.sub(r"21\.[ .0-9]*", "", check)
        check = re.sub(r"20\.[ .0-9]*", "", check)
        check = re.sub(r"19\.[ .0-9]*", "", check)
        if re.search(r"[0-9]\.[0-9]", check):
            dump = True
            sys.stdout.write(line)
        if re.search(r"\b[0-9][0-9][0-9][0-9][0-9]q*\b", check):
            dump = True
            sys.stdout.write(line)
if dump:
    sys.stdout.write(saved)

        
