#!/bin/bash
# Generate a correct notification for a test conversion.
# Meant to be called from the Makefile.

date=`(cd emacs-git >/dev/null; git log -1 --format=%ci)`
cat <<EOF
To: emacs-devel@gnu.org
Subject: New review conversion of Emacs repository

Next review version of the repository conversion is here:

	$1

This one is current to complete to ${date}.
It includes the transition patch. It has been repacked.

Cloning this may appear at first sight to fetch only the master branch,
but do "git branch -a" to list the other remote branches.  You can
easily create local tracking branches for those you are interested in.

The conversion machinery, including the lift script, can be cloned from

	git://gitorious.org/emacs-transition/emacs-transition.git

How you can help:

1. Examine the transition  patch (last commit) for errors and problems.

2. Use gitk or git log to spot CVS cliques consisting of uncoalesced
single-file changes and their ChangeLog entry commits; report them so
I can squash them into proper changesets. 

3. Investigate the old-branches and other-branches groups and determine
which branches, if any, should be discarded.

4. (Advanced) Identify the actual deletion points of as many of the
=-prefixed attic files as possible.  Look for the string "ATTIC
FILES" in emacs.lift and try to turn 'attic' calls into 'deattic'
calls. (I've already done this, but it's a difficult and finicky process;
I might have missed some.)

5. (Advanced) Learn the reposurgeon DSL.  Use this knowledge to audit
emacs.lift for correctness.

Barring any glitches found by people (other than me) doing these
auditing steps, the conversion procedure is pretty much ready to go.
I rerun it occasionally to catch new bzr references in the logs and
comments.  Note that a full conversion takes about ten hours of
compute time; this constraint needs to be planned around in the 
conversion-day schedule.
EOF
