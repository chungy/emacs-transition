# This is a reposurgeon script that sanity-checks a property of the Emacs repo.
# It is expected that stripped.fi was produced by stripping blobs out of
# attic.fi to preserve the structure.

read <stripped.fi

exec <<EOF

def sanecheck(repository, selection):
    for i in selection:
    	event = repository.events[i]
	for fileop1 in event.fileops:
	    if fileop1.op == 'M':
	        base1 = fileop1.path.split('/')[-1]
	        if base1.startswith('='):
		    for fileop2 in event.fileops:
			if fileop2.op == 'M':
			   base2 = fileop2.path.split('/')[-1]
			   if fileop2.path == fileop1.path[1:]:
			       if fileop1.ref != fileop2.ref:
			           print("%s %s %s %s" % (event.committer.action_stamp(), fileop2.path, fileop1.ref, fileop2.ref))
EOF

=C eval sanecheck(_repository,_selection)


